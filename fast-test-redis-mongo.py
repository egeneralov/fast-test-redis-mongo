import os
import redis
import pymongo
import socket


hostname = os.environ.get("HOSTNAME", "hostname") + ": "

def test_mongo_srv(uri):
  try:
    client = pymongo.MongoClient(uri)
    [ i for i in client.list_databases() ]
    return True
  except pymongo.errors.ServerSelectionTimeoutError:
    return False



def test_mongo():
  mongo_servers = os.environ.get("MONGO_SERVERS","").split(",")
  try:
    mongo_servers.remove('')
  except ValueError:
    pass
  
  print(
    hostname + "mongo: servers " + str(mongo_servers)
  )
  
  for srv in mongo_servers:
    print(hostname + "mongo: " + srv + ": started")
    if test_mongo_srv("mongodb://" + srv + ":27017"):
      print(hostname + "mongo: " + srv + ": pass")
    else:
      print(hostname + "mongo: " + srv + ": fail")



def test_redis_srv(host, port):
  port = int(port)
  r = redis.Redis(host=host, port=port, db=0)
  return r.ping()


def test_redis():
  redis_servers = os.environ.get("REDIS_SRV","").split(",")
  try:
    redis_servers.remove('')
  except ValueError:
    pass
  
  print(
    hostname + "redis: servers " + str(redis_servers)
  )
  
  for srv in redis_servers:
    host = srv.split(":")[0]
    try:
      port = srv.split(":")[1]
    except IndexError:
      port = 6379

    print(hostname + "redis: " + srv + ": started")
    if test_redis_srv(host, port):
      print(hostname + "redis: " + srv + ": pass")
    else:
      print(hostname + "redis: " + srv + ": fail")


def isPortOpened(host, port):
  port = int(port)
  sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  result = sock.connect_ex((host, port))
  if result == 0:
    sock.close()
    return True
  else:
    sock.close()
    return False

def test_hostport():
  hostport_servers = os.environ.get("HOSTPORT","").split(",")
  try:
    hostport_servers.remove('')
  except ValueError:
    pass
  
  print(
    hostname + "hostport: servers " + str(hostport_servers)
  )
  
  for srv in hostport_servers:
    host = srv.split(":")[0]
    try:
      port = srv.split(":")[1]
    except IndexError:
      port = 6379

    print(hostname + "hostport: " + srv + ": started")
    if isPortOpened(host, port):
      print(hostname + "hostport: " + srv + ": pass")
    else:
      print(hostname + "hostport: " + srv + ": fail")


if __name__ == '__main__':
  while True:
    print(hostname + "tests started")
    test_mongo()
    test_redis()
    test_hostport()
    print(hostname + "tests finished")


