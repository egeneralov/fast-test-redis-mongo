FROM python:3.7
RUN pip install pymongo redis pyinstaller
ADD fast-test-redis-mongo.py /
RUN pyinstaller -F fast-test-redis-mongo.py

FROM debian:buster-slim
COPY --from=0 /dist/fast-test-redis-mongo /bin/
CMD /bin/fast-test-redis-mongo
